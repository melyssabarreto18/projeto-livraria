<?php

namespace Database\Seeders;

use App\Models\Genero;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class generosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $generos = ['Romance', 'Ação', 'Terror', 'Comédia', 'Suspense', 'Ficção Científica', 'Drama', 'Documentário'];
        foreach ($generos as $key => $genero){
            $tipo = new Genero();
            $tipo->nome_genero = $genero;
            $tipo->save();
        }
    }
}
