<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vendas_livros', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_livro');
            $table->unsignedBigInteger('id_venda');
            $table->bigInteger('quantidade');
            $table->string('subtotal');
            $table->timestamps();

            $table->foreign('id_livro')->references('id')->on('livros');
            $table->foreign('id_venda')->references('id')->on('vendas');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vendas_livros');
    }
};
