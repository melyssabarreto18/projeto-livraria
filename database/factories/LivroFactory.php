<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class LivroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'titulo' => fake('pt_BR')->text(20),
            'preco' => fake('pt_BR')->numberBetween(20, 120),
            'descricao' => fake('pt_BR')->realText,
            'estoque' => fake('pt_BR')->numberBetween(50, 100),
            'id_genero' => fake('pt_BR')->numberBetween(1, 8)
        ];
    }
}
