<div class="grid grid-cols-2 gap-4 p-2">
    <div>
        <x-input-label for="name" :value="__('Nome')" />
        <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="$user->name ?? old('name')" required autofocus autocomplete="name" />
        <x-input-error class="mt-2" :messages="$errors->get('name')" />
    </div>

    <div>
        <x-input-label for="email" :value="__('Email')" />
        <x-text-input id="email" name="email" type="email" class="mt-1 block w-full" :value="$user->email ??old('email')" required autocomplete="username" />
        <x-input-error class="mt-2" :messages="$errors->get('email')" />
    </div>

    <div>
        <x-input-label for="senha" :value="__('Senha')" />
        <x-text-input id="senha" name="senha" type="password" class="mt-1 block w-full" :value="old('senha')" required autocomplete="username" />
        <x-input-error class="mt-2" :messages="$errors->get('senha')" />
    </div>

    <div class="grid grid-cols-2 gap-2">
        <div>
            <x-input-label for="telefone" :value="__('Telefone')" />
            <x-text-input id="telefone" name="telefone" type="text" class="mt-1 block w-full" :value="$user->telefone ?? old('telefone')" required autofocus autocomplete="name" />
            <x-input-error class="mt-2" :messages="$errors->get('telefone')" />
        </div>

        <div>
            <x-input-label for="cargo" :value="__('Cargo')" />
            <select name="cargo" id="cargo" class="mt-1 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg block w-full p-2.5">
                <option selected disabled>Escolha...</option>
                <option value="Autor">Autor</option>
                <option value="Cliente">Cliente</option>
            </select>
        </div>
    </div>
</div>

<div class="flex">
    <div class="items-center gap-4 p-2">
        <a href="{{ route('users.index') }}" class="button mb-2 mr-2 px-4 py-2 font-semibold text-sm bg-red-500 text-white rounded-lg shadow-sm flex">
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-skip-backward-btn w-5 h-5 mr-1" viewBox="0 0 16 16">
                <path d="M11.21 5.093A.5.5 0 0 1 12 5.5v5a.5.5 0 0 1-.79.407L8.5 8.972V10.5a.5.5 0 0 1-.79.407L5 8.972V10.5a.5.5 0 0 1-1 0v-5a.5.5 0 0 1 1 0v1.528l2.71-1.935a.5.5 0 0 1 .79.407v1.528l2.71-1.935z"/>
                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
              </svg>
            Voltar
        </a>
    </div>
    
    <div class="items-center gap-4 p-2">
        <button type="submit" class="button mb-2 mr-2 px-4 py-2 font-semibold text-sm bg-green-500 text-white rounded-lg shadow-sm flex">
            Salvar
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-save-fill fill-white w-5 h-5 ml-1" viewBox="0 0 16 16">
                <path d="M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v7.793L4.854 6.646a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l3.5-3.5a.5.5 0 0 0-.708-.708L8.5 9.293V1.5z"/>
            </svg>
        </button>
    </div>
</div>