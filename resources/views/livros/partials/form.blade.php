
<div class="grid grid-cols-3 gap-4 p-2">
    <div>
        <x-input-label for="titulo" :value="__('Titulo')" />
        <x-text-input id="titulo" name="titulo" type="text" class="mt-1 block w-full" :value="$livro->titulo ?? old('titulo')" required autofocus />
        <x-input-error class="mt-2" :messages="$errors->get('titulo')" />
    </div>

    <div>
        <x-input-label for="preço" :value="__('Preço (R$)')" />
        <x-text-input id="preco" name="preco" type="text" class="mt-1 block w-full" :value="$livro->preco ??old('email')" required/>
        <x-input-error class="mt-2" :messages="$errors->get('preço')" />
    </div>

    <div>
        <x-input-label for="estoque" :value="__('Estoque')" />
        <x-text-input id="estoque" name="estoque" type="text" class="mt-1 block w-full" :value="$livro->estoque ?? old('estoque')" required/>
        <x-input-error class="mt-2" :messages="$errors->get('estoque')" />
    </div>
</div>
<div class="grid flex p-2">
    <div>
        <x-input-label for="descricao" :value="__('Descrição')" />
        <textarea id="descricao" name="descricao" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Descrição" required>
            {{$livro->descricao ?? old('descricao')}}
        </textarea>
    </div>
</div>
<div class="flex p-2">
    <select id="genero" name="generos" class="flex-shrink-0 z-10 inline-flex items-center py-2.5 px-1 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-l-lg hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700 dark:text-white dark:border-gray-600">
        <option selected disabled>Genero...</option>
        @foreach($generos as $genero)
            <option value="{{ $genero->id }}">{{ $genero->nome_genero }}</option>
        @endforeach
    </select>
    <label for="simple-search" class="sr-only">Genero</label>
    <div class="relative w-full">
        <input type="text" name="genero" id="simple-search" class="block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-r-lg border-l-gray-50 border-l-2 border border-gray-300" placeholder="Novo Genero">
    </div>
</div>

<div class="flex">
    <div class="items-center gap-4 p-2">
        <a href="{{ route('users.index') }}" class="button mb-2 mr-2 px-4 py-2 font-semibold text-sm bg-red-500 text-white rounded-lg shadow-sm flex">
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-skip-backward-btn w-5 h-5 mr-1" viewBox="0 0 16 16">
                <path d="M11.21 5.093A.5.5 0 0 1 12 5.5v5a.5.5 0 0 1-.79.407L8.5 8.972V10.5a.5.5 0 0 1-.79.407L5 8.972V10.5a.5.5 0 0 1-1 0v-5a.5.5 0 0 1 1 0v1.528l2.71-1.935a.5.5 0 0 1 .79.407v1.528l2.71-1.935z"/>
                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
              </svg>
            Voltar
        </a>
    </div>

    <div class="items-center gap-4 p-2">
        <button type="submit" class="button mb-2 mr-2 px-4 py-2 font-semibold text-sm bg-green-500 text-white rounded-lg shadow-sm flex">
            Salvar
            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-save-fill fill-white w-5 h-5 ml-1" viewBox="0 0 16 16">
                <path d="M8.5 1.5A1.5 1.5 0 0 1 10 0h4a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h6c-.314.418-.5.937-.5 1.5v7.793L4.854 6.646a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l3.5-3.5a.5.5 0 0 0-.708-.708L8.5 9.293V1.5z"/>
            </svg>
        </button>
    </div>
</div>
