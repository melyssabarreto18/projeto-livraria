<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight flex">
            {{ __('Detalhes do Livro') }}
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-book fill-black w-6 h-6 ml-1" viewBox="0 0 16 16">
                <path d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
            </svg>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="grid grid-cols-3 gap-4 p-2">
                    <div>
                        <x-input-label for="titulo" :value="__('Titulo')" />
                        <x-text-input id="titulo" name="titulo" type="text" class="mt-1 block w-full" :value="$livro->titulo ?? old('titulo')" autofocus disabled/>
                        <x-input-error class="mt-2" :messages="$errors->get('titulo')" />
                    </div>

                    <div class="grid grid-cols-2 gap-4">
                        <div>
                            <x-input-label for="preço" :value="__('Preço (R$)')" />
                            <x-text-input id="preco" name="preco" type="text" class="mt-1 block w-full" :value="$livro->preco ??old('email')" disabled/>
                            <x-input-error class="mt-2" :messages="$errors->get('preço')" />
                        </div>

                        <div>
                            <x-input-label for="estoque" :value="__('Estoque')" />
                            <x-text-input id="estoque" name="estoque" type="text" class="mt-1 block w-full" :value="$livro->estoque ?? old('estoque')" disabled/>
                            <x-input-error class="mt-2" :messages="$errors->get('estoque')" />
                        </div>
                    </div>
                    <div>
                        <x-input-label for="genero" :value="__('Genero')" />
                        <x-text-input id="genero" name="genero" type="text" class="mt-1 block w-full" :value="$genero->nome_genero ?? old('genero')" disabled/>
                        <x-input-error class="mt-2" :messages="$errors->get('genero')" />
                    </div>
                </div>
                <div class="grid flex p-2">
                    <div>
                        <x-input-label for="descricao" :value="__('Descrição')" />
                        <textarea id="descricao" name="descricao" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300" placeholder="Descrição" disabled>
                            {{$livro->descricao ?? old('descricao')}}
                        </textarea>
                    </div>
                </div>
                <div class="flex">
                    <div class="items-center gap-4 p-2">
                        <a href="{{ route('users.index') }}" class="button mb-2 mr-2 px-4 py-2 font-semibold text-sm bg-red-500 text-white rounded-lg shadow-sm flex">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-skip-backward-btn w-5 h-5 mr-1" viewBox="0 0 16 16">
                                <path d="M11.21 5.093A.5.5 0 0 1 12 5.5v5a.5.5 0 0 1-.79.407L8.5 8.972V10.5a.5.5 0 0 1-.79.407L5 8.972V10.5a.5.5 0 0 1-1 0v-5a.5.5 0 0 1 1 0v1.528l2.71-1.935a.5.5 0 0 1 .79.407v1.528l2.71-1.935z"/>
                                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm15 0a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                            </svg>
                            Voltar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
