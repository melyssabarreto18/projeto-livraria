<?php

use App\Http\Controllers\LivrosController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->middleware(['verified'])->name('dashboard');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::prefix('users')->group(function () {
        Route::get('index', [UserController::class, 'index'])->name('users.index');
        Route::get('create', [UserController::class, 'create'])->name('users.create');
        Route::get('edit/{id}', [UserController::class, 'edit'])->name('users.edit');

        Route::post('search', [UserController::class, 'search'])->name('users.search');
        Route::post('store', [UserController::class, 'store'])->name('users.store');
        Route::put('update/{id}', [UserController::class, 'update'])->name('users.update');
        Route::delete('delete/{id}', [UserController::class, 'delete'])->name('users.destroy');
    });

    Route::prefix('livros')->group(function () {
        Route::get('index', [LivrosController::class, 'index'])->name('livros.index');
        Route::get('show/{id}', [LivrosController::class, 'show'])->name('livros.show');
        Route::get('create', [LivrosController::class, 'create'])->name('livros.create');
        Route::get('edit/{id}', [LivrosController::class, 'edit'])->name('livros.edit');

        Route::post('add/{id}', [LivrosController::class, 'add'])->name('livros.add');
        Route::post('buy/{id}', [LivrosController::class, 'buy'])->name('livros.buy');
        Route::post('search', [LivrosController::class, 'search'])->name('livros.search');
        Route::post('store', [LivrosController::class, 'store'])->name('livros.store');
        Route::put('update/{id}', [LivrosController::class, 'update'])->name('livros.update');
        Route::delete('delete/{id}', [LivrosController::class, 'delete'])->name('livros.destroy');
    });

    Route::prefix('vendas')->group(function (){
        Route::post('venda/{id}', [LivrosController::class, 'buy'])->name('livros.buy');
    });
});

require __DIR__.'/auth.php';
