<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livro_Venda extends Model
{
    use HasFactory;
    protected $table = 'vendas_livros';
    protected $fillable = [
        'id_livro',
        'id_venda',
        'quantidade',
        'subtotal'
    ];
}
