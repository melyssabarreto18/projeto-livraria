<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Livro extends Model
{
    use HasFactory;
    protected $fillable = [
        'titulo',
        'descricao',
        'preco',
        'estoque',
        'id_genero'
    ];

    public function livros()
    {
        return DB::table('livros')
            ->select(
                'livros.*',
                'generos.nome_genero'
            )
            ->join('generos', 'generos.id', '=', 'livros.id_genero')
            ->orderBy('id', 'desc')
            ->paginate();
    }

    public function search($titulo, $genero)
    {
        return DB::table('livros')
            ->select(
                'livros.*',
                'generos.nome_genero'
            )
            ->where('livros.titulo', 'LIKE', '%'.$titulo.'%')
            ->where('generos.id', 'LIKE', $genero)
            ->join('generos', 'generos.id', '=', 'livros.id_genero')
            ->orderBy('id', 'desc')
            ->paginate();
    }
}
