<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Venda extends Model
{
    use HasFactory;
    protected $fillable = [
        'data_venda',
        'total_venda',
        'id_user'
    ];

    public function carrinho($user_id)
    {
        return DB::table('vendas')
            ->select(
                DB::raw('sum(vendas_livros.quantidade) as quantidade'),
                DB::raw('sum(vendas_livros.subtotal) as preco')
            )
            ->join('vendas_livros', 'vendas_livros.id_venda', '=', 'vendas.id')
            ->where('vendas.id_user', $user_id)
            ->where('vendas.status', false)
            ->first();
    }
}
