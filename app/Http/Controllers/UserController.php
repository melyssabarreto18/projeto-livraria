<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::where('id', "!=", 1)->latest()->paginate();
        return view('users.index', [
            'users' => $users,
            'message' => session()->get('message')
        ]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'telefone' => $request->telefone,
            'cargo' => $request->cargo
        ]);

        return redirect()->route('users.index')->with('message', 'Usuário Criado!');
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        if(!$user)
            return back()->withErrors('Usuário Inexistente');
        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        if(!$user)
            return back()->withErrors('Usuário Inexistente');

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'telefone' => $request->telefone,
            'cargo' => $request->cargo
        ]);

        return redirect()->route('users.index')->with('message', 'Usuário Atualizado!');
    }

    public function delete($id)
    {
        User::destroy($id);
        return redirect()->route('users.index')->with('message', 'Usuário Deletado!');
    }

    public function search(Request $request)
    {
        $users = (new User())->search($request->filter);
        return view('users.index', [
            'users' => $users,
            'message' => false
        ]);
    }
}
