<?php

namespace App\Http\Controllers;

use App\Models\Genero;
use App\Models\Livro;
use App\Models\Livro_Venda;
use App\Models\Venda;
use Illuminate\Http\Request;

class LivrosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $livros = (new Livro())->livros();
        $generos = Genero::all();

        return view('livros.index', [
            'livros' => $livros,
            'generos' => $generos,
            'message' => session()->get('message')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $generos = Genero::all();
        return view('livros.create', [
            'generos' => $generos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $genero = $request->generos;
        if(!$request->generos){
            $genero = Genero::create([
                'nome_genero' => $request->genero
            ]);
        }

        Livro::create([
            'titulo' => $request->titulo,
            'descricao' => $request->descricao,
            'preco' => $request->preco,
            'estoque' => $request->estoque,
            'id_genero' => ($genero->id ?? $genero)
        ]);

        return redirect()->route('livros.index')->with('message', 'Livro Criado!');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $livro = Livro::where('id', $id)->first();
        $genero = Genero::where('id', $livro->id_genero)->first();
        if(!$livro)
            return back()->withErrors('Livro Inexistente');
        return view('livros.show', [
            'livro' => $livro,
            'genero' => $genero
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $livro = Livro::where('id', $id)->first();
        $generos = Genero::all();
        if(!$livro)
            return redirect()->back()->withErrors('Livro Inexistente');
        return view('livros.edit', [
            'livro' => $livro,
            'generos' => $generos
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $livro = Livro::where('id', $id)->first();
        if(!$livro)
            return redirect()->back()->withErrors('Livro Inexistente');

        $genero = $request->generos;
        if(!$request->generos){
            $genero = Genero::create([
                'nome_genero' => $request->genero
            ]);
        }

        $livro->update([
            'titulo' => $request->titulo,
            'descricao' => $request->descicao,
            'preco' =>$request->preco,
            'estoque' => $request->estoque,
            'id_genero' => ($genero->id ?? $genero)
        ]);

        return redirect()->route('livros.index')->with('message', 'Livro Atualizado!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete($id)
    {
        if(Livro_Venda::where('id_livro', $id)->get())
            return redirect()->back()->withErrors('Não é possível excluir.');

        Livro::destroy($id);

        return redirect()->route('livros.index')->with('message', 'Livro Deletado!');
    }

    public function search(Request $request)
    {
        $livros = (new Livro())->search($request->filter, $request->genero);
        $generos = Genero::all();

        return view('livros.index', [
            'livros' => $livros,
            'generos' => $generos,
            'message' => false
        ]);
    }

    public function add($id)
    {
        $livro = Livro::where('id', $id)->first();
        $venda = Venda::where('id_user', auth()->id())->where('status', false)->first();
        if($venda){
            $livro_venda = Livro_Venda::where('id_livro', $id)->where('id_venda', $venda->id)->first();
            if($livro_venda){
                $livro_venda->update([
                    'quantidade' => $livro_venda->quantidade + 1,
                    'subtotal' => ($livro_venda->subtotal + $livro->preco)
                ]);
            }else{
                Livro_Venda::create([
                    'id_livro' => $id,
                    'id_venda' => $venda->id,
                    'quantidade' => 1,
                    'subtotal' => $livro->preco
                ]);
            }
        }else{
            $venda = Venda::create([
                'data_venda' => new \DateTime(),
                'total_venda' => 1,
                'id_user' => auth()->id()
            ]);

            Livro_Venda::create([
                'id_livro' => $id,
                'id_venda' => $venda->id,
                'quantidade' => 1,
                'subtotal' => $livro->preco
            ]);
        }

        return redirect()->route('livros.index')->with('message', 'Livro Adicionado!');
    }

    public function buy($venda_id)
    {
        Venda::where('id', $venda_id)->update([
            'status' => true
        ]);
        return redirect()->route('dashboard');
    }
}
